package by.anegin.wearblocks.common.model

class AvailableMoves(private val rows: Int, private val columns: Int) {

	// кол-во шагов, на которые может переместиться каждая ячейка для каждого направления
	var movesCountLeft = Array(rows) { IntArray(columns) }
	var movesCountRight = Array(rows) { IntArray(columns) }
	var movesCountUp = Array(rows) { IntArray(columns) }
	var movesCountDown = Array(rows) { IntArray(columns) }

	// макс.количество шагов для каждого направления
	var maxMovesCountLeft: Int = 0
	var maxMovesCountRight: Int = 0
	var maxMovesCountUp: Int = 0
	var maxMovesCountDown: Int = 0

	// кол-во блоков отдельно по строкам/столбцам, которые будут удалены при движении в каждую сторону
	var blocksCanBeRemovedLeft = IntArray(rows)
	var blocksCanBeRemovedRight = IntArray(rows)
	var blocksCanBeRemovedUp = IntArray(columns)
	var blocksCanBeRemovedDown = IntArray(columns)

	// общее количество блоков, которые будут удалены при движении в каждую сторону
	var blocksCanBeRemovedLeftTotal: Int = 0
	var blocksCanBeRemovedRightTotal: Int = 0
	var blocksCanBeRemovedUpTotal: Int = 0
	var blocksCanBeRemovedDownTotal: Int = 0

	fun calculate(blocks: Array<Array<Block?>>) {
		maxMovesCountLeft = 0
		maxMovesCountRight = 0
		maxMovesCountUp = 0
		maxMovesCountDown = 0
		for (i in 0 until rows) {
			for (j in 0 until columns) {
				movesCountLeft[i][j] = availableMovesCount(blocks, i, j, Direction.LEFT)
				movesCountRight[i][j] = availableMovesCount(blocks, i, j, Direction.RIGHT)
				movesCountUp[i][j] = availableMovesCount(blocks, i, j, Direction.UP)
				movesCountDown[i][j] = availableMovesCount(blocks, i, j, Direction.DOWN)
				if (movesCountLeft[i][j] > maxMovesCountLeft)
					maxMovesCountLeft = movesCountLeft[i][j]
				if (movesCountRight[i][j] > maxMovesCountRight)
					maxMovesCountRight = movesCountRight[i][j]
				if (movesCountUp[i][j] > maxMovesCountUp) maxMovesCountUp = movesCountUp[i][j]
				if (movesCountDown[i][j] > maxMovesCountDown)
					maxMovesCountDown = movesCountDown[i][j]
			}
		}

		blocksCanBeRemovedLeftTotal = 0
		for (r in 0 until rows) {
			blocksCanBeRemovedLeft[r] = 0
			var c = 0
			while (c < columns && (blocks[r][c] == null || blocks[r][c]!!.color === Direction.LEFT.color)) {
				if (blocks[r][c] != null && blocks[r][c]!!.color === Direction.LEFT.color) {
					blocksCanBeRemovedLeft[r]++
				}
				c++
			}
			blocksCanBeRemovedLeftTotal += blocksCanBeRemovedLeft[r]
		}

		blocksCanBeRemovedRightTotal = 0
		for (r in 0 until rows) {
			blocksCanBeRemovedRight[r] = 0
			var c = columns - 1
			while (c >= 0 && (blocks[r][c] == null || blocks[r][c]!!.color === Direction.RIGHT.color)) {
				if (blocks[r][c] != null && blocks[r][c]!!.color === Direction.RIGHT.color) {
					blocksCanBeRemovedRight[r]++
				}
				c--
			}
			blocksCanBeRemovedRightTotal += blocksCanBeRemovedRight[r]
		}

		blocksCanBeRemovedUpTotal = 0
		for (c in 0 until columns) {
			blocksCanBeRemovedUp[c] = 0
			var r = 0
			while (r < rows && (blocks[r][c] == null || blocks[r][c]!!.color === Direction.UP.color)) {
				if (blocks[r][c] != null && blocks[r][c]!!.color === Direction.UP.color) {
					blocksCanBeRemovedUp[c]++
				}
				r++
			}
			blocksCanBeRemovedUpTotal += blocksCanBeRemovedUp[c]
		}

		blocksCanBeRemovedDownTotal = 0
		for (c in 0 until columns) {
			blocksCanBeRemovedDown[c] = 0
			var r = rows - 1
			while (r >= 0 && (blocks[r][c] == null || blocks[r][c]!!.color === Direction.DOWN.color)) {
				if (blocks[r][c] != null && blocks[r][c]!!.color === Direction.DOWN.color) {
					blocksCanBeRemovedDown[c]++
				}
				r--
			}
			blocksCanBeRemovedDownTotal += blocksCanBeRemovedDown[c]
		}
	}

	// на сколько может сдвинуться блок в указанном направлении
	private fun availableMovesCount(blocks: Array<Array<Block?>>, row: Int, column: Int, direction: Direction?): Int {
		if (direction != null
				&& row >= 0 && row < rows && column >= 0 && column < columns
				&& blocks[row][column] != null) {

			var line: Array<Block?>? = null
			when (direction) {
				Direction.LEFT -> {
					line = arrayOfNulls(column + 1)
					System.arraycopy(blocks[row], 0, line, 0, line.size)
				}
				Direction.UP -> {
					line = arrayOfNulls(row + 1)
					for (i in line.indices) line[i] = blocks[i][column]
				}
				Direction.RIGHT -> {
					line = arrayOfNulls(columns - column)
					for (i in line.indices)
						line[i] = blocks[row][columns - i - 1]
				}
				Direction.DOWN -> {
					line = arrayOfNulls(rows - row)
					for (i in line.indices)
						line[i] = blocks[rows - i - 1][column]
				}
			}

			var shrinkedCount = shrinkNulls(line)
			shrinkedCount = shrinkColor(line, shrinkedCount, direction.color)
			return line.size - shrinkedCount
		}
		return 0
	}

	// удаляет нулевые ячейки, сжимает массив к началу
	// возвращает количество непустых ячеек в исходном массиве
	private fun shrinkNulls(line: Array<Block?>): Int {
		var count = line.size
		var i = 0
		while (i < count) {
			if (line[i] == null) {
				if (i + 1 < count) {
					System.arraycopy(line, i + 1, line, i, count - i - 1)
				}
				count--
			} else {
				i++
			}
		}
		return count
	}

	// удаляет ячейки с указанным цветом color с начала массива до count
	// возвращает количество оставшихся ячеек
	private fun shrinkColor(line: Array<Block?>, count: Int, color: Colors): Int {
		var i = 0
		while (i < count && line[i]!!.color === color) i++
		System.arraycopy(line, i, line, 0, count - i)
		return count - i
	}
}