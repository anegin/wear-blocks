package by.anegin.wearblocks

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.anegin.wearblocks.common.model.Table
import by.anegin.wearblocks.common.view.TableView

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val table = Table(6, 6)
		table.initRandom()

		val tableView = findViewById<TableView>(R.id.table_view)
		tableView.setTable(table)
	}

}