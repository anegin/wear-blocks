package by.anegin.wearblocks.common.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.os.SystemClock
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewConfiguration
import android.view.animation.DecelerateInterpolator
import by.anegin.wearblocks.common.R
import by.anegin.wearblocks.common.model.Direction
import by.anegin.wearblocks.common.model.FallingBlock
import by.anegin.wearblocks.common.model.Table
import java.util.*

class TableView : View, View.OnTouchListener {

	companion object {
		private const val ANIM_DURATION = 250
	}

	private enum class State {
		IDLE, DRAG, FLING
	}

	private val blockPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)

	private val leftEdgePaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
	private val rightEdgePaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
	private val topEdgePaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
	private val bottomEdgePaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)

	private lateinit var table: Table

	private var state = State.IDLE

	private var dragDir: Direction? = null
	private var dragOffs = 0f
	private var maxDragOffs = 0f

	private var velocityTracker: VelocityTracker? = null
	private var minimumFlingVelocity = 0
	private var maximumFlingVelocity = 0
	private var touchSlop = 0

	private var downX = 0f
	private var downY = 0f

	private var blockSpacing = 0f

	private var tableWidth = 0f
	private var tableHeight = 0f

	private var edgeWidth = 0f
	private var edgeSpacing = 0f

	private val leftEdgePath = Path()
	private val rightEdgePath = Path()
	private val topEdgePath = Path()
	private val bottomEdgePath = Path()

	private val rect = RectF()

	private var currentBlurSize = 0f
	private var currentBlurDirection: Direction? = null

	private val fallingBlocks = ArrayList<FallingBlock>()

	private var tableRows = 0
	private var tableColumns = 0

	private var tableLeft = 0f
	private var tableTop = 0f

	private var blockWidth = 0f
	private var blockHeight = 0f
	private var blockRoundRect = edgeWidth / 2f

	private val rnd = Random()

	constructor(context: Context) : super(context) {
		init(context, null, 0)
	}

	constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
		init(context, attrs, 0)
	}

	constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
		init(context, attrs, defStyleAttr)
	}

	private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
		setLayerType(View.LAYER_TYPE_SOFTWARE, null)
		setOnTouchListener(this)

		val configuration = ViewConfiguration.get(context)
		touchSlop = configuration.scaledTouchSlop
		minimumFlingVelocity = configuration.scaledMinimumFlingVelocity
		maximumFlingVelocity = configuration.scaledMaximumFlingVelocity

		val a = context.obtainStyledAttributes(attrs, R.styleable.TableView, defStyleAttr, 0)
		var rows = a.getInt(R.styleable.TableView_table_rows, 0)
		var columns = a.getInt(R.styleable.TableView_table_columns, 0)
		tableWidth = a.getDimension(R.styleable.TableView_table_width, -1f)
		tableHeight = a.getDimension(R.styleable.TableView_table_height, -1f)
		blockSpacing = a.getDimension(R.styleable.TableView_blocks_spacing, 0f)
		edgeWidth = a.getDimension(R.styleable.TableView_edge_width, 0f)
		edgeSpacing = a.getDimension(R.styleable.TableView_edge_spacing, 0f)
		a.recycle()

		if (rows <= 0) rows = 1
		if (columns <= 0) columns = 1
		if (rows > 0 && columns > 0) {
			table = Table(rows, columns)
			table.initRandom()
		}

		blockPaint.style = Paint.Style.FILL

		leftEdgePaint.style = Paint.Style.FILL
		leftEdgePaint.color = Direction.LEFT.color.get(context)

		rightEdgePaint.style = Paint.Style.FILL
		rightEdgePaint.color = Direction.RIGHT.color.get(context)

		topEdgePaint.style = Paint.Style.FILL
		topEdgePaint.color = Direction.UP.color.get(context)

		bottomEdgePaint.style = Paint.Style.FILL
		bottomEdgePaint.color = Direction.DOWN.color.get(context)
	}

	fun setTable(table: Table) {
		this.table = table
		calculateSizes()
		invalidate()
	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec)
		calculateSizes()
	}

	private fun calculateSizes() {
		val width = measuredWidth
		val height = measuredHeight
		if (width == 0 || height == 0) return

		var tw = tableWidth
		var th = tableHeight
		if (tw <= 0 || th <= 0) {
			th = Math.min(width, height).toFloat()
			tw = th
		}
		tableLeft = (width - tw) / 2f
		tableTop = (height - th) / 2f

		tableRows = table.rows
		tableColumns = table.columns

		val totalHorizontalSpacing = (tableColumns - 1) * blockSpacing
		val totalVerticalSpacing = (tableRows - 1) * blockSpacing
		blockWidth = (tw - totalHorizontalSpacing) / tableColumns
		blockHeight = (th - totalVerticalSpacing) / tableRows

		val edgeW = (tw + 2f * (edgeWidth + edgeSpacing)).toInt()
		val edgeH = (th + 2f * (edgeWidth + edgeSpacing)).toInt()
		makeLeftEdgePath(edgeH.toFloat())
		makeRightEdgePath(edgeW.toFloat(), edgeH.toFloat())
		makeTopEdgePath(edgeW.toFloat())
		makeBottomEdgePath(edgeW.toFloat(), edgeH.toFloat())

		blockRoundRect = edgeWidth / 2f
	}

	public override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		updateEdgesMaskFilter(blockWidth, blockHeight)

		canvas.save()
		canvas.translate(tableLeft - edgeWidth - edgeSpacing, tableTop - edgeWidth - edgeSpacing)
		canvas.drawPath(leftEdgePath, leftEdgePaint)
		canvas.drawPath(rightEdgePath, rightEdgePaint)
		canvas.drawPath(topEdgePath, topEdgePaint)
		canvas.drawPath(bottomEdgePath, bottomEdgePaint)
		canvas.restore()

		canvas.save()
		canvas.translate(tableLeft, tableTop)
		var x: Float
		var y: Float
		val blockW = blockWidth + blockSpacing
		val blockH = blockHeight + blockSpacing
		val maxOffsForMoveLeft = dragOffs / table.availableMoves.maxMovesCountLeft
		val maxOffsForMoveRight = dragOffs / table.availableMoves.maxMovesCountRight
		val maxOffsForMoveUp = dragOffs / table.availableMoves.maxMovesCountUp
		val maxOffsForMoveDown = dragOffs / table.availableMoves.maxMovesCountDown
		val context = context

		for (i in 0 until tableRows) {
			for (j in 0 until tableColumns) {
				val block = table.getBlock(i, j) ?: continue
				x = j * blockW
				y = i * blockH
				if (state != State.IDLE) {
					when (dragDir) {
						Direction.LEFT -> x -= maxOffsForMoveLeft * table.availableMoves.movesCountLeft[i][j]
						Direction.RIGHT -> x += maxOffsForMoveRight * table.availableMoves.movesCountRight[i][j]
						Direction.UP -> y -= maxOffsForMoveUp * table.availableMoves.movesCountUp[i][j]
						Direction.DOWN -> y += maxOffsForMoveDown * table.availableMoves.movesCountDown[i][j]
					}
				}
				blockPaint.color = block.color!!.get(context)
				rect.set(x, y, x + blockWidth, y + blockHeight)
				canvas.drawRoundRect(rect, blockRoundRect, blockRoundRect, blockPaint)
			}
		}
		canvas.restore()

		if (fallingBlocks.size > 0) {
			val halfBlockWidth = blockWidth / 2f
			val halfBlockHeight = blockHeight / 2f
			for (block in fallingBlocks) {
				canvas.save()
				canvas.translate(block.x + halfBlockWidth, block.y + halfBlockHeight)
				canvas.rotate(block.rotation)
				canvas.scale(block.scale, block.scale)
				rect.set(-halfBlockWidth, -halfBlockHeight, halfBlockWidth, halfBlockHeight)
				blockPaint.color = block.color
				blockPaint.alpha = (255f * block.alpha).toInt()
				canvas.drawRoundRect(rect, blockRoundRect, blockRoundRect, blockPaint)
				canvas.restore()
			}
		}
		blockPaint.alpha = 255

		if (fallingBlocks.size > 0) {
			calculateFallingBlocks()
			invalidate()
		}
	}

	private fun makeLeftEdgePath(height: Float) {
		leftEdgePath.reset()
		val outDiameter = 2f * (edgeWidth + edgeSpacing)
		val inDiameter = 2f * edgeWidth

		rect.set(0f, 0f, outDiameter, outDiameter)
		leftEdgePath.addArc(rect, 180f, 45f)

		var x = (edgeWidth * Math.cos(Math.toRadians(45.0))).toFloat()
		var y = (edgeWidth * Math.sin(Math.toRadians(45.0))).toFloat()
		leftEdgePath.rLineTo(x, y)

		rect.set(edgeWidth, edgeWidth, edgeWidth + inDiameter, edgeWidth + inDiameter)
		leftEdgePath.arcTo(rect, 225f, -45f)

		leftEdgePath.rLineTo(0f, height - outDiameter)

		rect.set(edgeWidth, height - edgeWidth - inDiameter, edgeWidth + inDiameter, height - edgeWidth)
		leftEdgePath.arcTo(rect, 180f, -45f)

		x = (edgeWidth * Math.cos(Math.toRadians(135.0))).toFloat()
		y = (edgeWidth * Math.sin(Math.toRadians(135.0))).toFloat()
		leftEdgePath.rLineTo(x, y)

		rect.set(0f, height - outDiameter, outDiameter, height)
		leftEdgePath.arcTo(rect, 135f, 45f)

		leftEdgePath.close()
	}

	private fun makeRightEdgePath(width: Float, height: Float) {
		rightEdgePath.reset()
		val outDiameter = 2f * (edgeWidth + edgeSpacing)
		val inDiameter = 2f * edgeWidth

		rect.set(width - outDiameter, 0f, width, outDiameter)
		rightEdgePath.addArc(rect, 315f, 45f)

		rightEdgePath.rLineTo(0f, height - outDiameter)

		rect.set(width - outDiameter, height - outDiameter, width, height)
		rightEdgePath.arcTo(rect, 0f, 45f)

		val dx = (edgeWidth * Math.cos(Math.toRadians(225.0))).toFloat()
		val dy = (edgeWidth * Math.sin(Math.toRadians(225.0))).toFloat()
		rightEdgePath.rLineTo(dx, dy)

		rect.set(width - edgeWidth - inDiameter, height - edgeWidth - inDiameter, width - edgeWidth, height - edgeWidth)
		rightEdgePath.arcTo(rect, 45f, -45f)

		rightEdgePath.rLineTo(0f, -(height - outDiameter))

		rect.set(width - edgeWidth - inDiameter, edgeWidth, width - edgeWidth, edgeWidth + inDiameter)
		rightEdgePath.arcTo(rect, 0f, -45f)

		rightEdgePath.close()
	}

	private fun makeTopEdgePath(width: Float) {
		topEdgePath.reset()
		val outDiameter = 2f * (edgeWidth + edgeSpacing)
		val inDiameter = 2f * edgeWidth

		rect.set(0f, 0f, outDiameter, outDiameter)
		topEdgePath.addArc(rect, 225f, 45f)

		topEdgePath.rLineTo(width - outDiameter, 0f)

		rect.set(width - outDiameter, 0f, width, outDiameter)
		topEdgePath.arcTo(rect, 270f, 45f)

		val dx = (edgeWidth * Math.cos(Math.toRadians(135.0))).toFloat()
		val dy = (edgeWidth * Math.sin(Math.toRadians(135.0))).toFloat()
		topEdgePath.rLineTo(dx, dy)

		rect.set(width - edgeWidth - inDiameter, edgeWidth, width - edgeWidth, edgeWidth + inDiameter)
		topEdgePath.arcTo(rect, 315f, -45f)

		topEdgePath.rLineTo(-(width - outDiameter), 0f)

		rect.set(edgeWidth, edgeWidth, edgeWidth + inDiameter, edgeWidth + inDiameter)
		topEdgePath.arcTo(rect, 270f, -45f)

		topEdgePath.close()
	}

	private fun makeBottomEdgePath(width: Float, height: Float) {
		bottomEdgePath.reset()
		val outDiameter = 2f * (edgeWidth + edgeSpacing)
		val inDiameter = 2f * edgeWidth

		rect.set(width - outDiameter, height - outDiameter, width, height)
		bottomEdgePath.addArc(rect, 45f, 45f)

		bottomEdgePath.rLineTo(-(width - outDiameter), 0f)

		rect.set(0f, height - outDiameter, outDiameter, height)
		bottomEdgePath.arcTo(rect, 90f, 45f)

		val dx = (edgeWidth * Math.cos(Math.toRadians(315.0))).toFloat()
		val dy = (edgeWidth * Math.sin(Math.toRadians(315.0))).toFloat()
		bottomEdgePath.rLineTo(dx, dy)


		rect.set(edgeWidth, height - edgeWidth - inDiameter, edgeWidth + inDiameter, height - edgeWidth)
		bottomEdgePath.arcTo(rect, 135f, -45f)

		bottomEdgePath.rLineTo(width - outDiameter, 0f)

		rect.set(width - edgeWidth - inDiameter, height - edgeWidth - inDiameter, width - edgeWidth, height - edgeWidth)
		bottomEdgePath.arcTo(rect, 90f, -45f)

		bottomEdgePath.close()
	}

	private fun updateEdgesMaskFilter(blockWidth: Float, blockHeight: Float) {
		var blurSize = 0f
		var dir: Direction? = null
		if (state == State.IDLE) {
			if (currentBlurSize > 0f && currentBlurDirection != null) {
				blurSize = currentBlurSize
				dir = currentBlurDirection
			}
		} else if (dragDir != null && dragOffs > 0) {
			var canMove = false
			when (dragDir) {
				Direction.LEFT -> canMove = table.availableMoves.blocksCanBeRemovedLeftTotal > 0
				Direction.RIGHT -> canMove = table.availableMoves.blocksCanBeRemovedRightTotal > 0
				Direction.UP -> canMove = table.availableMoves.blocksCanBeRemovedUpTotal > 0
				Direction.DOWN -> canMove = table.availableMoves.blocksCanBeRemovedDownTotal > 0
			}
			if (canMove) {
				val blockSize = if (dragDir === Direction.LEFT || dragDir === Direction.RIGHT) blockWidth else blockHeight
				blurSize = if (dragOffs < blockSize) dragOffs else blockSize
				dir = dragDir
			} else {
				blurSize = 0f
				dir = null
			}
		}

		currentBlurSize = blurSize
		currentBlurDirection = dragDir

		var leftMaskFilter: MaskFilter? = null
		var rightMaskFilter: MaskFilter? = null
		var topMaskFilter: MaskFilter? = null
		var bottomMaskFilter: MaskFilter? = null
		var leftColor = Direction.LEFT.color.get(context)
		var rightColor = Direction.RIGHT.color.get(context)
		var topColor = Direction.UP.color.get(context)
		var bottomColor = Direction.DOWN.color.get(context)
		if (dir != null) {
			val blockSize = if (dir === Direction.LEFT || dragDir === Direction.RIGHT) blockWidth else blockHeight
			val lightness = 1f + blurSize / blockSize
			when (dir) {
				Direction.LEFT -> {
					leftColor = colorBrightness(leftColor, lightness)
					leftMaskFilter = BlurMaskFilter(blurSize * 0.3f, BlurMaskFilter.Blur.SOLID)
				}
				Direction.RIGHT -> {
					rightColor = colorBrightness(rightColor, lightness)
					rightMaskFilter = BlurMaskFilter(blurSize * 0.3f, BlurMaskFilter.Blur.SOLID)
				}
				Direction.UP -> {
					topColor = colorBrightness(topColor, lightness)
					topMaskFilter = BlurMaskFilter(blurSize * 0.3f, BlurMaskFilter.Blur.SOLID)
				}
				Direction.DOWN -> {
					bottomColor = colorBrightness(bottomColor, lightness)
					bottomMaskFilter = BlurMaskFilter(blurSize * 0.3f, BlurMaskFilter.Blur.SOLID)
				}
			}
		}
		leftEdgePaint.maskFilter = leftMaskFilter
		rightEdgePaint.maskFilter = rightMaskFilter
		topEdgePaint.maskFilter = topMaskFilter
		bottomEdgePaint.maskFilter = bottomMaskFilter
		leftEdgePaint.color = leftColor
		rightEdgePaint.color = rightColor
		topEdgePaint.color = topColor
		bottomEdgePaint.color = bottomColor
	}

	override fun canScrollHorizontally(direction: Int) = true

	override fun canScrollVertically(direction: Int) = true

	override fun onTouch(view: View, event: MotionEvent): Boolean {

		if (velocityTracker == null) velocityTracker = VelocityTracker.obtain()
		velocityTracker?.addMovement(event)

		when (event.actionMasked) {
			MotionEvent.ACTION_DOWN -> if (state == State.IDLE) {
				downX = event.x
				downY = event.y
				dragDir = null
				dragOffs = 0f
				invalidate()
				return true
			}
			MotionEvent.ACTION_MOVE -> {
				val deltaX = event.x - downX
				val deltaY = event.y - downY
				if (state == State.IDLE) {
					val distance = deltaX * deltaX + deltaY * deltaY
					if (distance > touchSlop) {

						val canMoveLeft = table.availableMoves.maxMovesCountLeft > 0
						val canMoveRight = table.availableMoves.maxMovesCountRight > 0
						val canMoveUp = table.availableMoves.maxMovesCountUp > 0
						val canMoveDown = table.availableMoves.maxMovesCountDown > 0
						if (deltaX > 0) {
							// вправо
							when {
								deltaY > 0 -> // вправо или вниз
									dragDir = if (deltaX >= deltaY) {
										if (canMoveRight) Direction.RIGHT else if (canMoveDown) Direction.DOWN else null
									} else {
										if (canMoveDown) Direction.DOWN else if (canMoveRight) Direction.RIGHT else null
									}
								deltaY < 0 -> // вправо или вверх
									dragDir = if (deltaX >= -deltaY) {
										if (canMoveRight) Direction.RIGHT else if (canMoveUp) Direction.UP else null
									} else {
										if (canMoveUp) Direction.UP else if (canMoveRight) Direction.RIGHT else null
									}
								else -> // только вправо
									dragDir = if (canMoveRight) Direction.RIGHT else null
							}
						} else if (deltaX < 0) {
							// влево
							when {
								deltaY > 0 -> // влево или вниз
									dragDir = if (-deltaX >= deltaY) {
										if (canMoveLeft) Direction.LEFT else if (canMoveDown) Direction.DOWN else null
									} else {
										if (canMoveDown) Direction.DOWN else if (canMoveLeft) Direction.LEFT else null
									}
								deltaY < 0 -> // влево или вверх
									dragDir = if (-deltaX >= -deltaY) {
										if (canMoveLeft) Direction.LEFT else if (canMoveUp) Direction.UP else null
									} else {
										if (canMoveUp) Direction.UP else if (canMoveLeft) Direction.LEFT else null
									}
								else -> // только влево
									dragDir = if (canMoveLeft) Direction.LEFT else null
							}
						} else {
							// вверх или вниз
							if (deltaY > 0) {
								dragDir = if (canMoveDown) Direction.DOWN else null
							} else if (deltaY < 0) {
								dragDir = if (canMoveUp) Direction.UP else null
							}
						}

						if (dragDir != null) {
							state = State.DRAG
							var tw = tableWidth
							var th = tableHeight
							if (tw <= 0 || th <= 0) {
								th = Math.min(width, height).toFloat()
								tw = th
							}
							val rows = table.rows
							val columns = table.columns
							val totalHorizontalSpacing = (columns - 1) * blockSpacing
							val totalVerticalSpacing = (rows - 1) * blockSpacing
							val blockWidth = (tw - totalHorizontalSpacing) / columns
							val blockHeight = (th - totalVerticalSpacing) / rows
							when (dragDir) {
								Direction.LEFT -> maxDragOffs = table.availableMoves.maxMovesCountLeft * (blockWidth + blockSpacing)
								Direction.RIGHT -> maxDragOffs = table.availableMoves.maxMovesCountRight * (blockWidth + blockSpacing)
								Direction.UP -> maxDragOffs = table.availableMoves.maxMovesCountUp * (blockHeight + blockSpacing)
								Direction.DOWN -> maxDragOffs = table.availableMoves.maxMovesCountDown * (blockHeight + blockSpacing)
							}
						}
					}
				}
				if (state == State.DRAG) {
					when (dragDir) {
						Direction.LEFT -> dragOffs = -deltaX
						Direction.RIGHT -> dragOffs = deltaX
						Direction.UP -> dragOffs = -deltaY
						Direction.DOWN -> dragOffs = deltaY
					}
					if (dragOffs > maxDragOffs) dragOffs = maxDragOffs
					if (dragOffs < 0) dragOffs = 0f
				}
				invalidate()
				return true
			}

			MotionEvent.ACTION_UP -> {
				if (state == State.DRAG) {
					state = State.FLING
					val velocityTracker = this.velocityTracker
					velocityTracker?.computeCurrentVelocity(1000, maximumFlingVelocity.toFloat())
					val velocityX = velocityTracker?.getXVelocity(event.getPointerId(0)) ?: 0f
					val velocityY = velocityTracker?.getYVelocity(event.getPointerId(0)) ?: 0f
					when (dragDir) {
						Direction.LEFT -> if (velocityX < 0 && -velocityX > minimumFlingVelocity) {
							moveToNewPosition()
						} else {
							finishMove()
						}
						Direction.RIGHT -> if (velocityX > 0 && velocityX > minimumFlingVelocity) {
							moveToNewPosition()
						} else {
							finishMove()
						}
						Direction.UP -> if (velocityY < 0 && -velocityY > minimumFlingVelocity) {
							moveToNewPosition()
						} else {
							finishMove()
						}
						Direction.DOWN -> if (velocityY > 0 && velocityY > minimumFlingVelocity) {
							moveToNewPosition()
						} else {
							finishMove()
						}
					}
				}
				velocityTracker?.recycle()
				velocityTracker = null
				return true
			}
		}
		return false
	}

	private fun finishMove() {
		if (dragOffs >= maxDragOffs / 2f) {
			moveToNewPosition()
		} else {
			moveBack()
		}
	}

	private fun moveBack() {
		if (dragOffs == 0f) {
			state = State.IDLE
			invalidate()
		} else {
			state = State.FLING
			val anim = ValueAnimator.ofFloat(dragOffs, 0f)
			anim.duration = ANIM_DURATION.toLong()
			anim.addListener(object : AnimatorListenerAdapter() {
				override fun onAnimationEnd(animation: Animator) {
					state = State.IDLE
					invalidate()
				}
			})
			anim.addUpdateListener { valueAnimator ->
				dragOffs = valueAnimator.animatedValue as Float
				invalidate()
			}
			anim.start()
		}
	}

	private fun moveToNewPosition() {
		if (dragOffs == maxDragOffs) {
			animateFallingBlocks()
			hideBlur()

			table.moveBlocks(dragDir)
			state = State.IDLE
			invalidate()
		} else {
			state = State.FLING
			val anim = ValueAnimator.ofFloat(dragOffs, maxDragOffs)
			anim.duration = ANIM_DURATION.toLong()
			anim.addListener(object : AnimatorListenerAdapter() {
				override fun onAnimationEnd(animation: Animator) {
					animateFallingBlocks()
					hideBlur()

					table.moveBlocks(dragDir)
					state = State.IDLE
					invalidate()
				}
			})
			anim.interpolator = DecelerateInterpolator()
			anim.addUpdateListener { valueAnimator ->
				dragOffs = valueAnimator.animatedValue as Float
				invalidate()
			}
			anim.start()
		}
	}

	private fun hideBlur() {
		if (currentBlurSize > 0f && currentBlurDirection != null) {
			val blurAnim = ValueAnimator.ofFloat(currentBlurSize, 0f)
			blurAnim.duration = ANIM_DURATION.toLong()
			blurAnim.interpolator = DecelerateInterpolator()
			blurAnim.addUpdateListener { valueAnimator ->
				currentBlurSize = valueAnimator.animatedValue as Float
				invalidate()
			}
			blurAnim.start()
		}
	}

	private fun animateFallingBlocks() {
		var removedBlocksCount = 0
		var removedBlocks: IntArray? = null
		if (dragDir != null) {
			when (dragDir) {
				Direction.LEFT -> {
					removedBlocksCount = table.availableMoves.blocksCanBeRemovedLeftTotal
					removedBlocks = table.availableMoves.blocksCanBeRemovedLeft
				}
				Direction.RIGHT -> {
					removedBlocksCount = table.availableMoves.blocksCanBeRemovedRightTotal
					removedBlocks = table.availableMoves.blocksCanBeRemovedRight
				}
				Direction.UP -> {
					removedBlocksCount = table.availableMoves.blocksCanBeRemovedUpTotal
					removedBlocks = table.availableMoves.blocksCanBeRemovedUp
				}
				Direction.DOWN -> {
					removedBlocksCount = table.availableMoves.blocksCanBeRemovedDownTotal
					removedBlocks = table.availableMoves.blocksCanBeRemovedDown
				}
			}
		}
		if (removedBlocksCount == 0 || removedBlocks == null) return

		val width = width
		val height = height
		var tw = tableWidth
		var th = tableHeight
		if (tw <= 0 || th <= 0) {
			th = Math.min(width, height).toFloat()
			tw = th
		}
		for (i in removedBlocks.indices) {
			for (c in 0 until removedBlocks[i]) {
				val block = FallingBlock(dragDir!!.color.get(context), blockWidth, blockHeight)
				block.sr = (0.05f + rnd.nextFloat() * 0.25f) * if (rnd.nextFloat() > 0.5f) 1 else -1
				block.ss = rnd.nextFloat() * 0.05f
				block.sa = rnd.nextFloat() * 0.05f
				when (dragDir) {
					Direction.LEFT -> {
						block.x = tableLeft - edgeSpacing - blockWidth
						block.y = tableTop + i * (blockHeight + blockSpacing)
						block.sx = -(0.05f + rnd.nextFloat() * 0.05f)
						block.sy = -(0.1f + rnd.nextFloat() * 0.4f)
					}
					Direction.RIGHT -> {
						block.x = tableLeft + tw + edgeSpacing
						block.y = tableTop + i * (blockHeight + blockSpacing)
						block.sx = 0.05f + rnd.nextFloat() * 0.05f
						block.sy = -(0.1f + rnd.nextFloat() * 0.4f)
					}
					Direction.UP -> {
						block.x = tableLeft + i * (blockWidth + blockSpacing)
						block.y = tableTop - edgeSpacing - blockHeight
						block.sx = (0.1f + rnd.nextFloat() * 0.4f) * if (block.x < width / 2f) -1f else 1f
						block.sy = -(0.1f + rnd.nextFloat() * 0.3f)
					}
					Direction.DOWN -> {
						block.x = tableLeft + i * (blockWidth + blockSpacing)
						block.y = tableTop + th + edgeSpacing
						block.sx = (0.1f + rnd.nextFloat() * 0.4f) * if (block.x < width / 2f) -1f else 1f
						block.sy = 0.05f + rnd.nextFloat() * 0.1f
					}
				}
				fallingBlocks.add(block)
			}
		}
	}

	private fun calculateFallingBlocks() {
		val time = SystemClock.uptimeMillis()
		val width = width
		val height = height
		val blocksToRemove = ArrayList<FallingBlock>()
		for (block in fallingBlocks) {
			val timeElapsed = 0.001f * (time - block.time)
			block.x += block.sx / timeElapsed
			block.y += block.sy / timeElapsed
			block.rotation = (block.rotation + block.sr / timeElapsed) % 360f
			block.scale = block.scale - block.ss
			if (block.scale < 0f) block.scale = 0f
			block.alpha = block.alpha - block.sa
			if (block.alpha < 0f) block.alpha = 0f
			if (block.x > width || block.x + blockWidth <= 0
					|| block.y > height || block.y + blockHeight <= 0
					|| block.scale < 0.01f || block.alpha < 0.01f) {
				blocksToRemove.add(block)
			}
			block.sy += 0.0005f / timeElapsed
			block.time = time
		}
		if (blocksToRemove.size > 0) {
			fallingBlocks.removeAll(blocksToRemove)
		}
	}

	private fun colorBrightness(color: Int, factor: Float): Int {
		val hsv = FloatArray(3)
		hsv[2] *= factor
		if (factor > 0) hsv[1] /= factor
		Color.colorToHSV(color, hsv)
		return Color.HSVToColor(hsv)
	}

}