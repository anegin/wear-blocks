package by.anegin.wearblocks.common.model

import android.os.SystemClock

data class FallingBlock(
		val color: Int,
		val blockWidth: Float,
		val blockHeight: Float) {

	var x = 0f
	var y = 0f
	var rotation = 0f
	var scale = 1f
	var alpha = 1f

	var sx = 0f   // x-speed
	var sy = 0f   // y-speed
	var sr = 0f   // rotate-speed
	var ss = 0f   // scale-speed
	var sa = 0f   // alpha-speed

	var time = SystemClock.uptimeMillis()

}