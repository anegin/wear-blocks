package by.anegin.wearblocks.common.model

import android.content.Context
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import by.anegin.wearblocks.common.R

enum class Colors(private val colorResId: Int) {

	LEFT(R.color.wall_left),
	RIGHT(R.color.wall_right),
	TOP(R.color.wall_top),
	BOTTOM(R.color.wall_bottom);

	@ColorInt
	fun get(context: Context): Int {
		return ContextCompat.getColor(context, colorResId)
	}

}