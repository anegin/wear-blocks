package by.anegin.wearblocks.common.model

import java.util.*

class Table(val rows: Int, val columns: Int) {

	private val blocks = Array(rows) {
		arrayOfNulls<Block>(columns)
	}

	val availableMoves = AvailableMoves(rows, columns)

	fun initRandom() {
		val rnd = Random()
		val colors = Colors.values()
		var c = 0
		for (i in 0 until rows)
			for (j in 0 until columns)
				blocks[i][j] = Block(colors[rnd.nextInt(colors.size)], ++c)
		calculateAvailableMoves()
	}

	fun getBlock(row: Int, column: Int): Block? {
		return if (row in 0..(rows - 1) && column >= 0 && column < columns) blocks[row][column] else null
	}

	fun moveBlocks(direction: Direction?) {
		when (direction) {
			Direction.LEFT -> for (r in 0 until rows) {
				for (c in 0 until columns) {
					val availableMovesLeft = availableMoves.movesCountLeft[r][c]
					if (availableMovesLeft > 0) {
						if (c - availableMovesLeft >= 0) {
							blocks[r][c - availableMovesLeft] = blocks[r][c]
						}
						blocks[r][c] = null
					}
				}
			}
			Direction.UP -> for (c in 0 until columns) {
				for (r in 0 until rows) {
					val availableMovesUp = availableMoves.movesCountUp[r][c]
					if (availableMovesUp > 0) {
						if (r - availableMovesUp >= 0) {
							blocks[r - availableMovesUp][c] = blocks[r][c]
						}
						blocks[r][c] = null
					}
				}
			}
			Direction.RIGHT -> for (r in 0 until rows) {
				for (c in columns - 1 downTo 0) {
					val availableMovesRight = availableMoves.movesCountRight[r][c]
					if (availableMovesRight > 0) {
						if (c + availableMovesRight < columns) {
							blocks[r][c + availableMovesRight] = blocks[r][c]
						}
						blocks[r][c] = null
					}
				}
			}
			Direction.DOWN -> for (c in 0 until columns) {
				for (r in rows - 1 downTo 0) {
					val availableMovesDown = availableMoves.movesCountDown[r][c]
					if (availableMovesDown > 0) {
						if (r + availableMovesDown < rows) {
							blocks[r + availableMovesDown][c] = blocks[r][c]
						}
						blocks[r][c] = null
					}
				}
			}
		}
		calculateAvailableMoves()
	}

	private fun calculateAvailableMoves() {
		availableMoves.calculate(blocks)
	}

}