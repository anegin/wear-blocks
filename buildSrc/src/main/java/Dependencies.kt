import org.gradle.api.JavaVersion

object Versions {

	val minSdk = 18
	val minSdkWear1 = 23
	val minSdkWear2 = 25
	val targetSdk = 28
	val buildTools = "28.0.1"

	val java = JavaVersion.VERSION_1_8

	val kotlin = "1.2.51"

	val support = "1.0.0-beta01"

	val constraintLayout = "2.0.0-alpha1"

	val wearable = "2.3.0"
}

object Deps {

	val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"

	val supportCore = "androidx.core:core:${Versions.support}"
	val supportDesign = "com.google.android.material:material:${Versions.support}"
	val supportWear = "androidx.wear:wear:${Versions.support}"

	val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"

	val playServicesWearable = "com.google.android.gms:play-services-wearable:15.0.1"

	val wearable = "com.google.android.support:wearable:${Versions.wearable}"
	val wearableCompile = "com.google.android.wearable:wearable:${Versions.wearable}"

}
