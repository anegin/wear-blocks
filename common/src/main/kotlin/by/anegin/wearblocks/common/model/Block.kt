package by.anegin.wearblocks.common.model

class Block(
		val color: Colors?,
		val value: Int
) {

	override fun toString(): String {
		return color.toString() ?: "null"
	}

}