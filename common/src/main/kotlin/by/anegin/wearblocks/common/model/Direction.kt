package by.anegin.wearblocks.common.model

enum class Direction(val color: Colors) {

	LEFT(Colors.LEFT),
	RIGHT(Colors.RIGHT),
	UP(Colors.TOP),
	DOWN(Colors.BOTTOM)

}